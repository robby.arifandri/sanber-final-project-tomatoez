<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'food';
    protected $fillable = ["name", "desc", "photo", "user_id"];

    public function user() {
        return $this->belongsTo('\App\User');
    }

    public function category() {
        return $this->belongsTo('\App\Category');
    }

    public function reviews() {
        return $this->hasMany('\App\Review');
    }

    public function tags() {
        return $this->belongsToMany('\App\Tag');
    }
}
