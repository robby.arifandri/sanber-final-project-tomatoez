<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Food;
use App\Tag;
use App\Category;
use PhpParser\Node\Expr\PostDec;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;


class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foods = Food::all();
        // dd($foods);
        return view('food.index', compact('foods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = Category::all();
        return view('food.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:food|max:255'
            // 'name' => 'required|unique:posts|max:255',
        ]);

        $category = Category::find($request["category"]);
        $tags_arr = explode(',', $request["tag"]);
        $tag_ids = [];
        foreach($tags_arr as $tag_name) {
            $tag = Tag::firstOrCreate([
                'tag_name' => $tag_name
            ]);
            $tag_ids[] = $tag->id;
        }

        $food = Auth::user()->foods()->create([
            "name" => $request["name"],
            "desc" => $request["desc"],
            "photo" => $request["photo"],
        ]);

        $food->category()->associate($category);
        $food->tags()->sync($tag_ids);
        $food->save();

        Alert::success('Success', 'Food berhasil disimpan!');

        return redirect('/foods')->with('status', 'Food berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $food = Food::find($id);
        return view('food.show', compact('food'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $food = Food::find($id);
        $categories = Category::all();
        return view('food.edit', [
            "food" => $food,
            "categories" => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $food = Food::find($id);
        if($food->user->id != Auth::user()->id) {
            Alert::error('Error', 'Beda pemilik, food tidak bisa diubah!');
            return redirect('/foods')->with('status', 'Beda pemilik, food tidak bisa diubah!');
        }

        // $request->validate([

        // ]);
        
        $category = Category::find($request["category"]);
        $tags_arr = explode(',', $request["tag"]);
        $tag_ids = [];
        foreach($tags_arr as $tag_name) {
            $tag = Tag::firstOrCreate([
                'tag_name' => $tag_name
            ]);
            $tag_ids[] = $tag->id;
        }

        $food = Food::find($id);
        $food->update([
            "name" => $request['name'],
            "desc" => $request['desc'],
            "photo" => $request['photo'],
            "user_id" => Auth::id()
        ]);

        $food->category()->associate($category);
        $food->tags()->sync($tag_ids);
        $food->save();

        return redirect('/foods')->with('status', 'Food berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $food = Food::find($id);
        if($food->user->id != Auth::user()->id) {
            Alert::error('Error', 'Beda pemilik, food tidak bisa dihapus!');
            return redirect('/foods')->with('status', 'Beda pemilik, food tidak bisa dihapus!');
        }

        Food::destroy($id);
        return redirect('/foods')->with('status', 'Food berhasil dihapus!');
    }
}
