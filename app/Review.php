<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "reviews";
    protected $fillable = ["desc", "is_positive", "user_id", "food_id"];

    public function user() {
        return $this->belongsTo('\App\User');
    }

    public function food() {
        return $this->belongsTo('\App\Food');
    }


}
