@extends('master')

@push('script-head')
<!-- CKEditor -->
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
</script>
@endpush

@section('content')

<section class="blog-details">
    <div class="container">
        <div class="single-blog-page">
            <div>
                <h2>Tambah Food</h2>
                <form action="/foods" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama Makanan">
                        @error('name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="desc">Deskripsi</label>
                        <textarea id="cke-food-desc" name="desc" class="form-control">
                        {!! old('content', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat tellus, tristique in rutrum ac, molestie ac velit. Etiam sed dolor at libero imperdiet finibus. Praesent tincidunt, ante ac tempor rhoncus, est metus maximus magna, molestie aliquam elit magna a sapien.') !!}
                        </textarea>
                        <!-- <input type="text" class="form-control" name="desc" id="desc" placeholder="Masukkan Deskripsi"> -->
                        @error('desc')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="photo">Photo</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="lfm-food-photo" data-input="photo" data-preview="holder" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input id="photo" class="form-control" type="text" name="photo" placeholder="Pilih foto">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                        @error('photo')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <!-- TODO add category select option -->
                    <div class="form-group">
                        <label for="category">Category</label>
                        <a class="btn btn-sm btn-primary" href="/categories/create" role="button">Add new Category</a>
                        <select class="form-control" id="category" name="category">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @error('tag')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <!-- TODO add tags select2 -->
                    <!-- TODO remove default value -->
                    <div class="form-group">
                        <label for="tag">Tags</label>
                        <input type="text" class="form-control" name="tag" id="tag" placeholder="Masukkan tag (pisahkan dengan koma, tanpa spasi)" value="main,dessert">
                        @error('tag')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <a class="btn btn-danger" href="/foods" role="button">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@push('scripts')
<!-- Laravel File Manager -->
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    // var route_prefix = "laravel-filemanager";
    $('#lfm-food-photo').filemanager('image');
</script>

<!-- CKEditor -->
<script>
    CKEDITOR.replace('cke-food-desc', options);
</script>
@endpush