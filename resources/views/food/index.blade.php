@extends('master')

<!-- @section('title')
<h1>Daftar foods</h1>
@endsection -->

@section('content')

<div>
    <a href="/foods/create" class="btn btn-primary" style="margin: 10px">Tambah</a>
</div>

<!-- @if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif -->

<section class="blog-section">
    <div class="nice-scroll">
        <div class="blog-grid-warp">
            <div class="blog-grid-sizer"></div>
            @forelse ($foods as $food)
            <div class="blog-grid">
                <div class="blog-item">
                    @empty($food->photo)
                    <img src="https://via.placeholder.com/150" alt="">
                    @endempty
                    <img src="{{$food->photo}}" alt="">

                    @empty($food->category->id)
                        <div class="bi-tag">uncategorized</div>
                    @else
                        <div class="bi-tag">{{$food->category->name}}</div>
                    @endempty

                    <div class="bi-text">
                        <!-- <div class="bi-date">May 19, 2019 | 3 Comment</div> -->
                        <h3><a href="/foods/{{$food->id}}">{{$food->name}}</a></h3>

                        @if(Auth::user()->id == $food->user_id)
                        <a href="/foods/{{$food->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/foods/{{$food->id}}" method="POST" style="display: inline">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            @empty
            <div style="margin-left: 20px">
                No data
            </div>
            @endforelse
        </div>
    </div>
</section>

@endsection