@extends('master')

@section('content')
<section class="blog-details">
    <div class="container">
        <div class="single-blog-page">
            <h2>{{$food->name}}</h2>
            <div class="blog-thumb">
                <div class="thumb-cata">{{$food->category->name}}</div>

                @empty($food->photo)
                <img src="https://via.placeholder.com/1024x300" alt="">
                @endempty
                <img src="{{ $food->photo }}" alt="">
            </div>
            <p>
                <strong>Tags</strong>
            </p>
            <p>
                @foreach($food->tags as $tag)
                    <button class="btn btn-primary btn-sm">{{$tag->tag_name}}</button>
                @endforeach
            </p>
            <p>
                {!!$food->desc!!}
            </p>
            <div class="comment-option">
                <h3>{{count($food->reviews)}} Review(s)</h3>
                @forelse ($food->reviews as $review)
                <div class="single-comment-item">
                    <div class="sc-author">
                        @empty($review->user->profile->picture)
                        <img src="img/blog-details/user-1.jpg" alt="">
                        <!-- <img src="https://via.placeholder.com/150" alt=""> -->
                        @endempty
                        <img src="{{$review->user->profile->picture}}" alt="">
                    </div>
                    <div class="sc-text">
                        <span>{{$review->created_at}}</span>
                        <h5>{{$review->user->name}}</h5>
                        <p>{{$review->desc}}</p>
                    </div>
                </div>
                @empty
                <p>No reviews</p>
                @endforelse
            </div>
            <div class="comment-form">
                <h3>Leave A Review</h3>
                <form action="/reviews" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" name="desc" id="desc" placeholder="Masukkan review">
                        <input type="hidden" class="form-control" name="food_id" id="food_id" value="{{$food->id}}">

                        @error('name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit">Post Review</button>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection