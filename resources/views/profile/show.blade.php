@extends('master')

@section('content')
<section class="blog-details">
    <div class="container">
        <div class="single-blog-page">
            <h2>User Setting</h2>
            <br>
            <div class="row">
                <div class="col">
                    <h3>Hi, {{Auth::user()->name}}</h3>
                    <p><strong>Email</strong></p>
                    <p>{{Auth::user()->email}}</p>

                    <p><strong>Short Biography</strong></p>
                    <p>
                        {!!$profile->bio!!}
                    </p>
                </div>
                <div class="col">
                    <div class="blog-thumb">
                        @empty($profile->picture)
                        <img src="https://via.placeholder.com/150x150" alt="">
                        @endempty
                        <img src="{{ $profile->picture }}" alt="" style="max-height: 150px">
                    </div>
                </div>
            </div>
            <a class="btn btn-success" href="/user/profile/edit" role="button">Edit</a>
            <a class="btn btn-danger" href="/foods" role="button">Back</a>
        </div>
    </div>
</section>

@endsection