@extends('master')

@push('script-head')
<!-- CKEditor -->
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
</script>
@endpush

@section('content')
<section class="blog-details">
    <div class="container">
        <div class="single-blog-page">
            <div>
                <h2>Edit User Profile</h2>
                <form action="/user/profile/edit" method="POST">
                    <!-- TODO add required validation -->
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}" id="name" placeholder="Masukkan nama" readonly>
                        @error('name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" name="email" value="{{Auth::user()->email}}" id="email" placeholder="Masukkan email" readonly>
                        @error('email')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="picture">Profile Picture</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a id="lfm-user-picture" data-input="picture" data-preview="holder" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input id="picture" class="form-control" type="text" name="picture" value="{{Auth::user()->profile->picture}}" placeholder="Pilih foto profil">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                        @error('picture')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="desc">Deskripsi</label>
                        <textarea id="cke-user-bio" name="bio" class="form-control">
                        {!! Auth::user()->profile->bio !!}
                        </textarea>
                        @error('desc')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Edit</button>
                    <a class="btn btn-danger" href="/user/profile" role="button">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@push('scripts')
<!-- Laravel File Manager -->
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    // var route_prefix = "laravel-filemanager";
    $('#lfm-user-picture').filemanager('image');
</script>

<!-- CKEditor -->
<script>
    CKEDITOR.replace('cke-user-bio', options);
</script>
@endpush