@extends('master')

@section('content')

<section class="blog-section">
    <div class="nice-scroll">
        <div class="blog-grid-warp">
            <div class="blog-grid-sizer"></div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/1.jpg" alt="">
                    <div class="bi-tag">people</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Female Body Shape Men Find</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/2.jpg" alt="">
                    <div class="bi-tag">nature</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Leica M10-D Is a Simplified Version Of The </a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/3.jpg" alt="">
                    <div class="bi-tag">camera</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">Art Photo Exhibition In America</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/4.jpg" alt="">
                    <div class="bi-tag">nature</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Biggest Cinema Event In 2019</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/5.jpg" alt="">
                    <div class="bi-tag">animal</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Most Expensive Cameras On The Planet</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/7.jpg" alt="">
                    <div class="bi-tag">People</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Biggest Cinema Event In 2019</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/9.jpg" alt="">
                    <div class="bi-tag">camera</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">Camera Type Digital Camera With Rangefinder</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/8.jpg" alt="">
                    <div class="bi-tag">nature</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The D-Lux 7 Is Leica's Brand New Premium Compact </a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/6.jpg" alt="">
                    <div class="bi-tag">camera</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Biggest Cinema Event In 2019</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/10.jpg" alt="">
                    <div class="bi-tag">animal</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">Meet David The Photographer</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/12.jpg" alt="">
                    <div class="bi-tag">nature</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">Camera Type Digital Camera With Rangefinder</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/14.jpg" alt="">
                    <div class="bi-tag">camera</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">Ahead Of Our Full Sony A6400 Review, We've </a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/11.jpg" alt="">
                    <div class="bi-tag">animal</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">A Unique Colourful And Elegant Design </a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/15.jpg" alt="">
                    <div class="bi-tag">nature</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Female Body Shape Men Find</a></h3>
                    </div>
                </div>
            </div>
            <div class="blog-grid">
                <div class="blog-item">
                    <img src="img/blog/13.jpg" alt="">
                    <div class="bi-tag">nature</div>
                    <div class="bi-text">
                        <div class="bi-date">May 19, 2019 | 3 Comment</div>
                        <h3><a href="blog-details.html">The Leica M10-D Is a Simplified Version Of The </a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection