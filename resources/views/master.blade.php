<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>Tomatoez | Food reviewing app</title>
	<meta charset="UTF-8">
	<meta name="description" content="Tomatoez Food Reviews">
	<meta name=" keywords" content="food, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}" />
	<link rel="stylesheet" href="{{asset('/css/themify-icons.css')}}" />
	<link rel="stylesheet" href="{{asset('/css/accordion.css')}}" />
	<link rel="stylesheet" href="{{asset('/css/fresco.css')}}" />
	<link rel="stylesheet" href="{{asset('/css/owl.carousel.min.css')}}" />

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="{{asset('/css/style.css')}}" />


	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	@stack('script-head')

</head>

<body>

	@include('partial.sidebar')
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>


	<!-- Offcanvas Menu Section end -->

	@include('partial.navbar')

	<!-- Hero, Item, or Item details Section -->
	@yield('content')
	<!-- Section End -->


	<!-- SweetAlert -->
	@include('sweetalert::alert')

	<!--====== Javascripts & Jquery ======-->
	<script src="{{asset('/js/vendor/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('/js/bootstrap.min.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="{{asset('/js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('/js/imagesloaded.pkgd.min.js')}}"></script>
	<script src="{{asset('/js/isotope.pkgd.min.js')}}"></script>
	<script src="{{asset('/js/jquery.nicescroll.min.js')}}"></script>
	<script src="{{asset('/js/circle-progress.min.js')}}"></script>
	<script src="{{asset('/js/pana-accordion.js')}}"></script>
	<script src="{{asset('/js/main.js')}}"></script>

	@stack('scripts')

</body>

</html>