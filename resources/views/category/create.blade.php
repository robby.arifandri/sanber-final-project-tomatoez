@extends('master')

@section('content')

<section class="blog-details">
    <div class="container">
        <div class="single-blog-page">
            <div>
                <h2>Tambah Kategori</h2>
                <form action="/categories" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan nama kategori">
                        @error('name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <a class="btn btn-danger" href="/foods" role="button">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection