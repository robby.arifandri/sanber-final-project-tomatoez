# Tomatoez

A food review Laravel app as Sanbercode Laravel Final Project submission Kelompok 5.

User can register to add foods and reviews of foods added by other users. 

## Getting Started

Clone this repository then run

```
composer install
```

Initialize application key
```
php artisan key:generate
```

Make sure you have PostgreSQL installed and set the config in `.env`. For example,
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=<your_db_name>
DB_USERNAME=postgres
DB_PASSWORD=<postgres_password>
```

For the file upload to function, set also the  `APP_URL`. For example
```
APP_URL=http://localhost:8000
```

## ERD 
![ERD](./public/img/erd.jpg "Logo Title Text 1")

## Front End Template
This app uses [Tulen](https://themewagon.com/themes/free-bootstrap-4-html5-photography-website-template-tulen/), a free Bootstrap HTML template, as the front end theme

## External Libraries, Packages, and Tools

- [Sweet Alert](https://github.com/realrashid/sweet-alert)
- [Laravel File Manager](https://unisharp.github.io/laravel-filemanager/)
- [Laravel CKEditor](https://github.com/UniSharp/laravel-ckeditor)
- [placeholder.com](https://placeholder.com/)
- [ui-avatars](https://ui-avatars.com)

## Deployment to Heroku
> Note: upload file or image function is not working on Heroku. 

Install heroku CLI and follow guide in [Heroku Laravel Tutorial](https://devcenter.heroku.com/articles/getting-started-with-laravel).

Create free PostgreSQL add ons, add its config as env variables for the Heroku instance.

Deploy it with

```
git push heroku
```

The deployed app can be accessed in https://sbtomatoez.herokuapp.com/


## Demo
Video Demo: https://youtu.be/TxlJlkiNe-0

## Members (Kelompok 5)

- Fachran Sandi (@tiedsandi)
- Robby Arifandri (@robbyarif)
- Ibnu Pujiono (@ibnupujiono)

## Future Works

- Replace laravel local storage for image upload with other object storage solution  
- Add more validation and tests


## License

This app is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
