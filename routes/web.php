<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FoodController@index')->middleware('auth');
//set home ke food index
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('foods', 'FoodController')->middleware('auth');
Route::resource('reviews', 'ReviewController')->middleware('auth');
Route::resource('categories', 'CategoryController')->middleware('auth');

// Route::resource('profiles', 'ProfileController')->middleware('auth');
Route::get('/user/profile', 'ProfileController@show')->middleware('auth');
Route::get('/user/profile/edit', 'ProfileController@edit')->middleware('auth');
Route::put('/user/profile/edit', 'ProfileController@update')->middleware('auth');


// Laravel File Manager
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});


// TODO remove example web and android content
Route::get('/konten/index', function () {
    return view('konten.index');
});
Route::get('/konten/blog', function () {
    return view('konten.blog');
});
Route::get('/konten/blog-details', function () {
    return view('konten.blog-details');
});
Route::get('/konten/contact', function () {
    return view('konten.contact');
});
Route::get('/konten/about', function () {
    return view('konten.about');
});

